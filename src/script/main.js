// 1) ForEach перебирає значення в масиві. Він для кожного значення викликає callback функцію і перебирає масив заданій умові;
// 2) Щоб видалити масив можна просто присвоїти йому значення null або [];
// Наприклад: arr = [];      arr = null;
//  3)Для цього є метода isArray()


let arr = ['hello', 'world', 23, '23', null]

const filterBy = (arr, type) => arr.filter(item => typeof item !== type)

console.log(filterBy(arr, "string"))
